package taskManager01;

public class Commands {

    final static String HELP = "help";
    final static String HELP_HELP = "help : Вывод доступных команд.";
    final static String PROJECT_CREATE = "project-create";
    final static String HELP_PROJECT_CREATE = "project-create: Создание нового проекта.";
    final static String PROJECT_CLEAR = "project-clear";
    final static String HELP_PROJECT_CLEAR = "project-clear: Удаление всех проектов.";
    final static String PROJECT_REMOVE = "project-remove";
    final static String HELP_PROJECT_REMOVE = "project-remove: Выборочное удаление проектов.";
    final static String PROJECT_LIST = "project-list";
    final static String HELP_PROJECT_LIST = "project-list: Вывод всех проектов.";
    final static String TASK_CREATE = "task-create";
    final static String HELP_TASK_CREATE = "task-create: Создание нового задания.";
    final static String TASK_CLEAR = "task-clear";
    final static String HELP_TASK_CLEAR = "task-clear: Удаление всех задач.";
    final static String TASK_REMOVE = "task-remove";
    final static String HELP_TASK_REMOVE = "task-remove: Выборочное удаление задач.";
    final static String TASK_LIST = "task-list";
    final static String HELP_TASK_LIST = "task-list: Вывод всех задач.";
    final static String FINISH = "finish";
    final static String HELP_FINISH = "finish: Остановка программы.";

}
