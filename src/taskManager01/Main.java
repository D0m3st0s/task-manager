package taskManager01;

import java.util.Scanner;

import static taskManager01.Commands.*;
import static taskManager01.Help.help;


public class Main {

    public static Scanner scr = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("       Добро Пожаловать в Task Manager");
        System.out.println("...ID Проекта Можно Найти в Списке Проектов...");
        while (true) {

            String commandName = scr.nextLine();

            if (commandName.equals(HELP)) {
                help();
            }
            if (commandName.equals(PROJECT_CREATE)) {
                System.out.println("Введите имя проекта:");
                String PrName = scr.nextLine();
                CreateProject.createProject(PrName);
            }
            if (commandName.equals(PROJECT_CLEAR)) {
                ProjectClear.projectClear();
            }
            if (commandName.equals(PROJECT_REMOVE)) {
                ProjectRemove.projectRemove();
            }
            if (commandName.equals(PROJECT_LIST)) {
                ProjectList.projectList();
            }
            if (commandName.equals(TASK_CREATE)) {
                CreateTask.createTask();
            }
            if (commandName.equals(TASK_CLEAR)) {
                TaskClear.taskClear();
            }
            if (commandName.equals(TASK_REMOVE)) {
                TaskRemove.taskRemove();
            }
            if (commandName.equals(TASK_LIST)) {
                TaskList.taskList();
            }
            if (commandName.equals(FINISH)) {
                break;
            }
        }
    }


}
